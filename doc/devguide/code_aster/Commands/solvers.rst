.. _commands-solvers:

#########################################
Non Linear Solver for Mechanical Analyses
#########################################

.. automodule:: code_aster.Solvers
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: code_aster.Solvers.problem_solver
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: code_aster.Solvers.non_linear_solver
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: code_aster.Solvers.physical_state
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: code_aster.Solvers.solver_features
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: code_aster.Solvers.convergence_manager
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: code_aster.Solvers.geometric_solver
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: code_aster.Solvers.snes_solver
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: code_aster.Solvers.incremental_solver
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: code_aster.Solvers.time_stepper
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: code_aster.Solvers.step_solver
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: code_aster.Solvers.storage_manager
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: code_aster.Solvers.logging_manager
   :show-inheritance:
   :members:
   :special-members: __init__
